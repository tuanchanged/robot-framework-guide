*** Settings ***
Library    lib/GGSearchLibrary.py 


*** Variables ***
${STATUS_CODE_KEY_STRING}          status_code
${ELAPSED_TIME_KEY_STRING}         elapsed_time
${RESULT_IN_DETAIL_KEY_STRING}     result_in_detail
${NUMBER_OF_RESULTS_KEY_STRING}    number_of_results
${STATUS_CODE_EXPECTED}            200


*** Test Cases ***
Verify user can search google with keyword as string   
    &{search_results}=    Google Search    Terralogic
    Should Be Equal As Integers    &{search_results}[${STATUS_CODE_KEY_STRING}]    ${STATUS_CODE_EXPECTED}
    Display result     &{search_results}
       

*** Keywords ***
Display result    [Arguments]    &{search_results}
    Log To Console    ${EMPTY}
    Log To Console    Successfully:    
    Log To Console    - The status code: &{search_results}[${STATUS_CODE_KEY_STRING}]
    Log To Console    - The elapsed time in seconds: &{search_results}[${ELAPSED_TIME_KEY_STRING}]
    Log To Console    - The number of results: &{search_results}[${NUMBER_OF_RESULTS_KEY_STRING}]
    Log To Console    - The results in detail: &{search_results}[${RESULT_IN_DETAIL_KEY_STRING}]
    
