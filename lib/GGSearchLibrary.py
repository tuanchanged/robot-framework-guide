# Google Doc: https://developers.google.com/custom-search/docs/xml_results
import requests
import re
import string
from bs4 import BeautifulSoup

"""
This function allows send a requests

parameters: url, params

returns: The response object
"""


def _send_request(url, params):
    with requests.session() as c:
        response = requests.get(url, params=params)
        response.raise_for_status()
        return response

""" 
This function will be sent a request to google server 

parameters: query, num

Returns: The response object
  
"""


def _send_google_search_request(query):
    _validate_input_search(query)
    params = {'q': query, 'num': 1, 'ie': 'utf8', 'oe': 'utf8' }
    return _send_request('https://www.google.com/search', params)


def _validate_input_search(query):
    if(query == ""):
        raise("Please Input The Keyword")
    
"""
This function will be get the elapsed time response

returns: elapsed time response in seconds 
"""


def _get_elapsed_time_response(response):
    return response.elapsed.total_seconds()

"""
This function to parse the content of the google response

parameters: HTML Content

returns: Dictionary includes:
    - The number of results related
    - The title of pages related
"""


def _parse_html(html_content):
      
    ''' Number of the related results '''
    regex_filter_numbers_and_doc = '\d.+\d'
    soup = BeautifulSoup(html_content, 'html.parser')
    numberOfResultsElement = soup.select('#resultStats')
    numberOfResults = numberOfResultsElement[0].getText()
    numberOfResults = re.findall(regex_filter_numbers_and_doc, numberOfResults)[0]
  
    ''' Results in detail '''
    resultElements = soup.select('h3.r > a')
    results = []
    for resultElement in resultElements:
        title = resultElement.getText()
        results.append(title)
       
    return {
            "number_of_results" : numberOfResults,
            "result_in_detail" : results 
            }

"""
This function to search keyword from google

parameters: keyword, num

returns: Dictionary includes:
    - The number of results related
    - The title of pages related
"""


def google_search(keyword):
    try:
        response = _send_google_search_request(keyword)
        data = _parse_html(response.content)

        return {
                "status_code" : response.status_code,
                "elapsed_time" : response.elapsed.total_seconds(),
                "number_of_results" : data["number_of_results"],
                "result_in_detail": data["result_in_detail"]
            }
    except Exception as err:
        raise('An exception happened: ' + str(err))




