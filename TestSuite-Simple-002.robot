*** Settings ***
Library           lib/GGSearchLibrary.py


*** Test Cases ***
Verify user can search google with keyword as string and using data-driven
    [Template]    Google Search
    Terralogic
    Python 3
    How to study Robot Framework
         
Verify user can search google with keyword as number and using data-driven
    [Template]    Google Search
    114
    115