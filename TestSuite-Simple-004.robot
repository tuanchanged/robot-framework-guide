*** Settings ***
Library    lib/GGSearchLibrary.py 


*** Variables ***
${STATUS_CODE_KEY_STRING}          status_code
${ELAPSED_TIME_KEY_STRING}         elapsed_time
${RESULT_IN_DETAIL_KEY_STRING}     result_in_detail
${NUMBER_OF_RESULTS_KEY_STRING}    number_of_results


*** Test Cases ***
Verify user can search google with keyword as string   
    &{search_results}=    Google Search    Terralogic
    Log To Console    ${EMPTY}
    Log To Console    &{search_results}[status_code]
    Log To Console    &{search_results}[elapsed_time]
    Log To Console    &{search_results}[number_of_results]
    Log To Console    &{search_results}[result_in_detail]
    
Verify user can search google with keyword as string with key of dict as variable
    &{search_results}=    Google Search    Terralogic
    Log To Console    ${EMPTY}
    Log To Console    &{search_results}[${STATUS_CODE_KEY_STRING}]
    Log To Console    &{search_results}[${ELAPSED_TIME_KEY_STRING}]
    Log To Console    &{search_results}[${RESULT_IN_DETAIL_KEY_STRING}]
    Log To Console    &{search_results}[${NUMBER_OF_RESULTS_KEY_STRING}]