*** Settings ***
Resource    resources/Resource.robot

*** Test Cases ***
Verify user can search google with keyword as string   
    &{search_results}=    Google Search    Terralogic
    Should Be Equal As Integers    &{search_results}[${STATUS_CODE_KEY_STRING}]    ${STATUS_CODE_EXPECTED}
    Display result     &{search_results}