*** Settings ***
Library           lib/GGSearchLibrary.py


*** Test Cases ***
Verify user can search google with keyword as string and using behavior-driven-development (BDD)
    Given a user has a keyword as Terralogic
    When user input that keyword to search
    Then user can search google successfully
    And user can get the search results 
         

*** Keywords ***
a user has a keyword as Terralogic
    Log To Console    HELLO: Given

user input that keyword to search
    Log To Console    HELLO: When

user can search google successfully
    Log To Console    HELLO: Then

user can get the search results
    Log To Console    HELLO: And